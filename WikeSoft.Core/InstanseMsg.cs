﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Core
{
    /// <summary>
    /// 操作结果 
    /// </summary>
    public class OperationResult
    {
        public bool Sucess { get; set; }

        public List<String> Errors { get; set; }

        public OperationResult()
        {
            Errors = new List<string>();
            Sucess = true;
        }
    }
}
