﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Core.License
{
    public class LicenseEntity
    {
        /// <summary>
        /// Key值
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 过期日期
        /// </summary>
        public DateTime ExpireDate { get; set; }
    }
}
