﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Enterprise.Entities
{
    public class DataModelInitializer : DropCreateDatabaseIfModelChanges<WikeDbContext> // CreateDatabaseIfNotExists<DataModelContext>
    {
        protected override void Seed(WikeDbContext context)
        {


            var roles = new List<SysRole>
            {
                new SysRole() {Id="067A9FFD-462F-4398-8006-2B5657CBDDE6",RoleName = "系统管理员",Remark = "系统管理员",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="A5208032-3C5B-4CDC-91E4-33331EFB7A55",RoleName = "流程设计",Remark = "流程设计",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="89ABE7D7-75A0-47D6-BA01-333839222119",RoleName = "总经理",Remark = "总经理",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="4997DC7A-BE5F-4256-AEF6-6CB321C73243",RoleName = "行政",Remark = "行政",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="E8DC5DBF-C753-4D08-BE50-B4E017209E74",RoleName = "普通员工",Remark = "普通员工",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="61D72D73-ACD4-4EF9-82BA-DD231B7253BC",RoleName = "部门主管",Remark = "部门主管",CreateDate = DateTime.Now,IsDelete = false},
                new SysRole() {Id="1230C652-B69B-4407-95FC-EBB7EB964265",RoleName = "项目经理",Remark = "项目经理",CreateDate = DateTime.Now,IsDelete = false},
            };
            context.SysRoles.AddRange(roles);
            context.SaveChanges();
            var pages = new List<SysPage>
            {
                new SysPage()
                {
                    Id =  "3D794E41-E1EE-4315-AAF1-4A7732169416",ParentId = null,PageCode = "100",PageName = "系统设置",PagePath = "#",Description = null,IsUsed = true,CssName = "fa-cog",PathCode = "AA",IsDelete = false
                   ,SysRoles = context.SysRoles.Where( x=>new [] { "067A9FFD-462F-4398-8006-2B5657CBDDE6","89ABE7D7-75A0-47D6-BA01-333839222119" ,"4997DC7A-BE5F-4256-AEF6-6CB321C73243" ,"E8DC5DBF-C753-4D08-BE50-B4E017209E74" ,"61D72D73-ACD4-4EF9-82BA-DD231B7253BC" ,"1230C652-B69B-4407-95FC-EBB7EB964265","A5208032-3C5B-4CDC-91E4-33331EFB7A55" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="4BC3B535-E1CB-4A3E-B1E0-89B7AF981796",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.01",PageName = "角色管理",PagePath = "/Role/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAA",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="C4DCE65B-44A2-4384-AE08-5D80A687A1CC",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.02",PageName = "部门管理",PagePath = "/Department/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAB",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="2A094905-4B7D-42D8-945F-FC6CE34E8A99",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.03",PageName = "功能管理",PagePath = "/Page/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAC",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="717EF415-189B-4CDD-A2B1-6DE8826D8493",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.04",PageName = "功能授权",PagePath = "/Role/SetRights",Description = null,IsUsed = true,CssName = null,PathCode = "AAAD",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="929D6813-1856-445E-A612-E804812877FC",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.05",PageName = "用户管理",PagePath = "/User/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAE",IsDelete = false
                   ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="21BFCC9C-CCD1-4CD4-A190-053137F234A3",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.06",PageName = "流程类别管理",PagePath = "/WorkFlowCategory/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAF",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="F9A43C22-EBEA-475D-BDE7-81CDCFAA7352",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.07",PageName = "流程设计",PagePath = "/FlowDesign/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAG",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6","89ABE7D7-75A0-47D6-BA01-333839222119" ,"4997DC7A-BE5F-4256-AEF6-6CB321C73243" ,"E8DC5DBF-C753-4D08-BE50-B4E017209E74" ,"61D72D73-ACD4-4EF9-82BA-DD231B7253BC" ,"1230C652-B69B-4407-95FC-EBB7EB964265","A5208032-3C5B-4CDC-91E4-33331EFB7A55" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="74B762A7-30B9-482F-B164-B658A4BA2271",ParentId ="3D794E41-E1EE-4315-AAF1-4A7732169416",PageCode = "100.08",PageName = "运行参数设置",PagePath = "/KeyValue/Index",Description = null,IsUsed = true,CssName = null,PathCode = "AAAH",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="D2429C03-0692-4DB6-A499-DD827F2A9915",ParentId = null,PageCode = "200",PageName = "流程管理",PagePath = "#",Description = null,IsUsed = true,CssName = "fa-commenting",PathCode = "AB",IsDelete = false
                     ,SysRoles = context.SysRoles.Where( x=>new [] {"89ABE7D7-75A0-47D6-BA01-333839222119","4997DC7A-BE5F-4256-AEF6-6CB321C73243" ,"E8DC5DBF-C753-4D08-BE50-B4E017209E74" ,"61D72D73-ACD4-4EF9-82BA-DD231B7253BC"  ,"1230C652-B69B-4407-95FC-EBB7EB964265" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="BA3AF546-24E0-4725-A253-E58595E18447",ParentId ="D2429C03-0692-4DB6-A499-DD827F2A9915",PageCode = "200.01",PageName = "请假申请",PagePath = "/Holiday/Index",Description = null,IsUsed = true,CssName = null,PathCode = "ABAA",IsDelete = false
                   ,SysRoles = context.SysRoles.Where( x=>new [] {"E8DC5DBF-C753-4D08-BE50-B4E017209E74" }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F",ParentId ="D2429C03-0692-4DB6-A499-DD827F2A9915",PageCode = "200.02",PageName = "请假审核",PagePath = "/Holiday/Audit",Description = null,IsUsed = true,CssName = null,PathCode = "ABAB",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"89ABE7D7-75A0-47D6-BA01-333839222119","4997DC7A-BE5F-4256-AEF6-6CB321C73243" ,"61D72D73-ACD4-4EF9-82BA-DD231B7253BC" ,"1230C652-B69B-4407-95FC-EBB7EB964265"  }.Contains(x.Id) ).ToList()
                },
                new SysPage()
                {
                    Id ="5B39998B-7322-48E3-94F8-AFC16FB79379",ParentId ="D2429C03-0692-4DB6-A499-DD827F2A9915",PageCode = "200.99",PageName = "与我相关",PagePath = "/WorkFlowCategory/HistoryListView",Description = null,IsUsed = true,CssName = null,PathCode = "ABAC",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"1230C652-B69B-4407-95FC-EBB7EB964265","61D72D73-ACD4-4EF9-82BA-DD231B7253BC" ,"E8DC5DBF-C753-4D08-BE50-B4E017209E74" ,"4997DC7A-BE5F-4256-AEF6-6CB321C73243"  ,"89ABE7D7-75A0-47D6-BA01-333839222119" }.Contains(x.Id) ).ToList()
                },
            };
            context.SysPages.AddRange(pages);
            context.SaveChanges();

            var departments = new List<SysDepartment>
            {
                new SysDepartment() {Id="4A7335F8-367E-4C06-82C0-4DB694DEC5E2",DepartmentName = "Wikesoft",ParentId =null,PathCode = "AA",IsDelete = false},
                new SysDepartment() {Id="884FC7B8-47A1-44FD-8970-4FF16A655E87",DepartmentName = "总经理办公室",ParentId ="4A7335F8-367E-4C06-82C0-4DB694DEC5E2",PathCode = "AAAA",IsDelete = false},
                new SysDepartment() {Id="59485231-7B38-4A95-BCDA-721F87A5BFBB",DepartmentName = "研发部",ParentId ="884FC7B8-47A1-44FD-8970-4FF16A655E87",PathCode = "AAAAAA",IsDelete = false},
                new SysDepartment() {Id="B0DF6F62-7068-4FA6-82B2-597494327937",DepartmentName = "研发一部",ParentId ="59485231-7B38-4A95-BCDA-721F87A5BFBB",PathCode = "AAAAAAAA",IsDelete = false},
                new SysDepartment() {Id="20CE56AA-0645-41FD-9FF6-51364D0A62F7",DepartmentName = "行政部",ParentId ="884FC7B8-47A1-44FD-8970-4FF16A655E87",PathCode = "AAAAAB",IsDelete = false},
            };
            context.SysDepartments.AddRange(departments);
            context.SaveChanges();
            var users = new List<SysUser>
            {
                new SysUser
                {
                    Id ="97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46", UserName = "研发部门主管", PassWord = "c4ca4238a0b923820dcc509a6f75849b",TrueName = "研发部门主管",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="59485231-7B38-4A95-BCDA-721F87A5BFBB",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"A5208032-3C5B-4CDC-91E4-33331EFB7A55","61D72D73-ACD4-4EF9-82BA-DD231B7253BC" }.Contains(x.Id) ).ToList()
                   
                },
                new SysUser
                {
                    Id ="C9028C6D-1AEB-473C-8105-4CF03C59766B", UserName = "一部项目经理", PassWord = "c4ca4238a0b923820dcc509a6f75849b",TrueName = "一部项目经理",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="B0DF6F62-7068-4FA6-82B2-597494327937",IsDelete = false
                     ,SysRoles = context.SysRoles.Where( x=>new [] {"A5208032-3C5B-4CDC-91E4-33331EFB7A55","1230C652-B69B-4407-95FC-EBB7EB964265" }.Contains(x.Id) ).ToList()

                   
                },
                new SysUser
                {
                    Id ="D34BE365-8F07-4FEE-9713-B079EC4AF9A3", UserName = "admin", PassWord = "54ec50c1d51814b4cb9f15af0347192a",TrueName = "系统管理员",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="4A7335F8-367E-4C06-82C0-4DB694DEC5E2",IsDelete = false
                     ,SysRoles = context.SysRoles.Where( x=>new [] {"067A9FFD-462F-4398-8006-2B5657CBDDE6" }.Contains(x.Id) ).ToList()

                     
                },
                new SysUser
                {
                    Id ="A22C56A2-78D7-4B61-8609-D3AD98876159", UserName = "总经理", PassWord = "c4ca4238a0b923820dcc509a6f75849b",TrueName = "总经理",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="884FC7B8-47A1-44FD-8970-4FF16A655E87",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"A5208032-3C5B-4CDC-91E4-33331EFB7A55","89ABE7D7-75A0-47D6-BA01-333839222119" }.Contains(x.Id) ).ToList()
                    
                },
                new SysUser
                {
                    Id ="64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4", UserName = "一部员工", PassWord = "c4ca4238a0b923820dcc509a6f75849b",TrueName = "一部员工",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="B0DF6F62-7068-4FA6-82B2-597494327937",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] {"A5208032-3C5B-4CDC-91E4-33331EFB7A55","E8DC5DBF-C753-4D08-BE50-B4E017209E74" }.Contains(x.Id) ).ToList()
                    
                },
                 new SysUser
                {
                    Id ="52906604-5DFB-47A4-AD75-663AB1504163", UserName = "行政", PassWord = "c4ca4238a0b923820dcc509a6f75849b",TrueName = "行政",UserStatus =  Enum.UserStatus.Enabled ,PasswordExpirationDate = DateTime.Now.AddYears(10),DepartmentId ="20CE56AA-0645-41FD-9FF6-51364D0A62F7",IsDelete = false
                    ,SysRoles = context.SysRoles.Where( x=>new [] { "4997DC7A-BE5F-4256-AEF6-6CB321C73243", "A5208032-3C5B-4CDC-91E4-33331EFB7A55" }.Contains(x.Id) ).ToList()

                },

            };
            context.SysUsers.AddRange(users);
            var keyvalues = new List<SysKeyValue>
            {
                new SysKeyValue() { Id="46AEFF2D-5DD4-4470-97B6-64757042EC59",CnName = "系统名称",Key = "SystemTitle",Memo = "系统名称",Value = "WikeFlow"}
            };

            context.SysKeyValues.AddRange(keyvalues);
            context.SaveChanges();
           

        }

    }
}
