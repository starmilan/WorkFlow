﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 借出状态（0：申请借出，1：已借出 ，2：驳回，3：归还申请，4：归还通过，5：归还驳回）
    /// </summary>
    public enum MaterialGoStatusEnum
    {

        /// <summary>
        /// 申请借出
        /// </summary>
        [Description("申请借出")]
        BorrowApply = 0,
        
        /// <summary>
        ///已借出
        /// </summary>
        [Description("已借出")]
        BorrowAgree = 1,

        /// <summary>
        /// 借出驳回
        /// </summary>
        [Description("借出驳回")]
        BorrowReject = 2,

        /// <summary>
        /// 已归还
        /// </summary>
        [Description("已归还")]
        Return = 3

     
    }
}
