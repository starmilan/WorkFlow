﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    public enum StoreTypeEnum
    {
        [Description("正常入库")]
        Normal =0,

        [Description("已调补资料")]
        AddInfomation =1
    }
}
