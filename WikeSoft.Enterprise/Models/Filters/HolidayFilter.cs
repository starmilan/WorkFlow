﻿using System.Collections.Generic;

namespace WikeSoft.Enterprise.Models.Filters
{
    public class HolidayFilter: BaseFilter
    {
        public string UserId { get; set; }
        public List<string> FlowIds { get; set; }
    }
}
