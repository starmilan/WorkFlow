﻿using System;
using WikeSoft.Data.Enum;
using WikeSoft.Data.Models.Filters;

namespace WikeSoft.Enterprise.Models.Filters.Sys
{
    public class TaskWakeFilter:BaseFilter
    {
        public Guid? UserId { get; set; }

        public TaskWakeStatusEnum? TaskWakeStatus { get; set; } // TaskWakeStatus
    }
}
