﻿namespace WikeSoft.Enterprise.Models
{
    /// <summary>
    /// 模型验证错误消息模版
    /// </summary>
    public class ModelErrorMessage
    {
        /// <summary>
        /// 必填信息
        /// </summary>
        public const string Required = "{0}不能为空";

        /// <summary>
        /// 最大长度信息
        /// </summary>
        public const string MaxLength = "{0}长度不能超过{1}个字符";

        /// <summary>
        /// 信息已存在
        /// </summary>
        public const string Exists = "{0}已存在";

        /// <summary>
        /// 数字信息
        /// </summary>
        public const string Number = "{0}必须是数字";

        /// <summary>
        /// 正则表达式格式信息
        /// </summary>
        public const string Regular = "{0}格式不正确";

        /// <summary>
        /// 正则表达式密码格式信息
        /// </summary>
        public const string RegularPassword = "{0}格式必须为字母和数字的组合,最小长度为8个字符";

        /// <summary>
        /// 两次输入不一致
        /// </summary>
        public const string NotSame = "两次输入不一致";

        /// <summary>
        /// 新密码不能和以前的一样
        /// </summary>
        public const string IsOldPassword = "新密码不能和以前的一样";

        /// <summary>
        /// 老密码错误
        /// </summary>
        public const string UnCorrectPassword = "{0}输入错误";
    }

    /// <summary>
    /// 常用的正则表达式
    /// </summary>
    public class RegularRule
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        public const string Email = @"^(\w)+(\.\w+)*@(\w)+((\.\w+)+)$";

        /// <summary>
        /// 手机号码
        /// </summary>
        public const string Phone = @"1[3-9]\d{9}";
    }
}
