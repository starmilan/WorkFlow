﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Enterprise.Models.WorkFlow
{
    /// <summary>
    /// 退证流程
    /// </summary>
    public class BackCertificateFlowModel
    {
        /// <summary>
        /// 人才证书Id
        /// </summary>
        public Guid CustomerCertificateId { get; set; }

        [Display(Name = "退证原因")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public String BackReason { get; set; }

        public Guid UserId { get; set; }
    }
}
