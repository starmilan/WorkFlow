﻿using System.Linq;
using AutoMapper;
using SimpleInjector;
using WikeSoft.Core.Extension;
using WikeSoft.Data;
using WikeSoft.Data.Models;
using WikeSoft.Data.Models.Sys;
using WikeSoft.Enterprise.Entities;
using WikeSoft.Enterprise.Models;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise
{
    /// <summary>
    /// 模块初始化
    /// </summary>
    public class WikeModuleInitializer : ModuleInitializer
    {
        /// <summary>
        /// 加载SimpleInject配置
        /// </summary>
        /// <param name="container"></param>
        public override void LoadIoc(Container container)
        {
            var registrations =
                from type in typeof (WikeModuleInitializer).Assembly.GetTypes()
                where
                    type.Namespace != null && (type.Namespace.IsNotBlank() &&
                                               //type.Namespace.StartsWith("WikeSoft.Data.AppServices") &&
                                               type.Namespace.StartsWith("WikeSoft.Enterprise.AppServices") && 
                                               type.GetInterfaces().Any(x => x.Name.EndsWith("Service")) &&
                                               type.GetInterfaces().Any())
                select new {Service = type.GetInterfaces().First(), Implementation = type};

            foreach (var reg in registrations)
            {
                container.Register(reg.Service, reg.Implementation, Lifestyle.Scoped);
            }
        }

        /// <summary>
        /// 加载AutoMapper配置
        /// </summary>
        public override void LoadMapper(IMapperConfigurationExpression cfg)
        {
            // todo 配置Entity和Model之间的映射关系

            #region 系统管理
            //角色
            cfg.CreateMap<SysRole, RoleModel>().ReverseMap();
            cfg.CreateMap<SysRole, RoleAddModel>().ReverseMap();
            cfg.CreateMap<SysRole, RoleEditModel>().ReverseMap();
            cfg.CreateMap<SysRole, ZTreeModel>()
                .ForMember(m => m.id, e => e.MapFrom(item => item.Id))
                .ForMember(m => m.name, e => e.MapFrom(item => item.RoleName));

            //运行参数
            cfg.CreateMap<SysKeyValue, KeyValueModel>().ReverseMap();
            cfg.CreateMap<SysKeyValue, KeyValueAddModel>().ReverseMap();
            cfg.CreateMap<SysKeyValue, KeyValueEditModel>().ReverseMap();

            //用户
            cfg.CreateMap<SysUser, UserModel>().ReverseMap();
            cfg.CreateMap<SysUser, UserAddModel>().ReverseMap();
            cfg.CreateMap<SysUser, UserEditModel>()
                .ForMember(x => x.PassWord, e => e.Ignore())
                .ReverseMap()
                .ForMember(x => x.PassWord, e => e.Ignore());

            //页面
            cfg.CreateMap<SysPage, NavTreeModel>()
                .ForMember(x => x.Id, e => e.MapFrom(x => x.Id))
                .ForMember(x => x.ParentId, e => e.MapFrom(x => x.ParentId))
                .ForMember(x => x.Name, e => e.MapFrom(x => x.PageName))
                .ForMember(x => x.Url, e => e.MapFrom(x => x.PagePath));
            cfg.CreateMap<SysPage, ZTreeModel>()
                .ForMember(m => m.id, e => e.MapFrom(item => item.Id))
                .ForMember(m => m.pId, e => e.MapFrom(item => item.ParentId))
                .ForMember(m => m.name, e => e.MapFrom(item => item.PageName));
            cfg.CreateMap<SysPage, PageModel>();
            cfg.CreateMap<SysPage, PageAddModel>().ReverseMap();
            cfg.CreateMap<SysPage, PageEditModel>().ReverseMap();
          
             
            //数据字典
            cfg.CreateMap<SysDataDictionay, DataDictionayModel>().ReverseMap();
            cfg.CreateMap<SysDataDictionay, DataDictionayAddModel>().ReverseMap();
            cfg.CreateMap<SysDataDictionay, DataDictionayEditModel>().ReverseMap();
           
            //附件
            cfg.CreateMap<SysAttachment, AttachmentModel>().ReverseMap();
            cfg.CreateMap<SysAttachment, AttachmentAddModel>().ReverseMap();

            //部门管理
            cfg.CreateMap<SysDepartment, DepartmentModel>().ReverseMap();
            cfg.CreateMap<SysDepartment, DepartmentAddModel>().ReverseMap();
            cfg.CreateMap<SysDepartment, DepartmentEditModel>().ReverseMap();

            #endregion




          


         

        }
    }
}
