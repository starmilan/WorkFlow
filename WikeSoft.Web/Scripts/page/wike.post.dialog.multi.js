﻿function openPostsDialog() {
    parent.layer.open({
        title: '岗位查询',
        type: 2,
        content: "/Post/QueryMulti",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var posts = $(layero).find("iframe")[0].contentWindow.getPosts();
            setPostDatas(posts);
            parent.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setPostDatas(posts) {
    //todo
}

$(document).ready(function () {
    $("#btnSelectPost").click(openPostsDialog);
});