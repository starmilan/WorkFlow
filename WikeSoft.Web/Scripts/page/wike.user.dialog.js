﻿function openPostDialog() {
    top.layer.open({
        title: '功能查询',
        type: 2,
        content: "/Department/ListView",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var tem = $(layero).find("iframe")[0].contentWindow.getContent();
            setData(tem.Id, tem.DepartmentName);
            top.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setData(id, departmentName) {
   
    $("#DepartmentId").val(id);
    $("#DepartmentName").val(departmentName);
}


$(document).ready(function () {
    $("#btnSelectPost").click(openPostDialog);
});