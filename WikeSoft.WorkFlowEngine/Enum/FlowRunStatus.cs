﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Enum
{
    public enum FlowRunStatus
    {

        /// <summary>
        /// 运行中
        /// </summary>
        Run = 1,



        /// <summary>
        /// 历史
        /// </summary>
        History = 0
    }
}
