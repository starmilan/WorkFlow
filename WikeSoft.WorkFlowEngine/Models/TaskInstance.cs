﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class TaskInstance
    {

        /// <summary>
        /// 流程定义ID
        /// </summary>
        public string WorkFlowDefId { get; set; }

        /// <summary>
        /// 流程定义key
        /// </summary>
        public string WorkFlowDefKey { get; set; }

        /// <summary>
        /// 流程定义名称
        /// </summary>
        public string WorkFlowDefName { get; set; }

        /// <summary>
        /// 实例ID
        /// </summary>
        public string InstanceId { get; set; }

        /// <summary>
        /// 节点定义名称
        /// </summary>
        public string NodeDefName { get; set; }

        /// <summary>
        /// 节点定义Id
        /// </summary>
        public string NodeDefId { get; set; }

        /// <summary>
        /// 节点ID
        /// </summary>
        public string TaskId { get; set; }

        /// <summary>
        /// 处理人ID
        /// </summary>
        public string DealUserId { get; set; }

        /// <summary>
        /// 处理人
        /// </summary>
        public string DealUser { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime? DealDate { get; set; }

        /// <summary>
        /// 处理意见
        /// </summary>
        public string DealRemark { get; set; }
        /// <summary>
        /// 流程实例的版本号
        /// </summary>
        public int FlowVersion { get; set; }
        /// <summary>
        /// 处理人
        /// </summary>
        public string TargetUser { get; set; }
    }
}
